#~/usr/bin/python
import math
import os

from flask import Blueprint, Flask, jsonify, request
from flask_httpauth import HTTPBasicAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from sqlalchemy import asc, desc
from werkzeug.security import check_password_hash, generate_password_hash

from database import db_session
from helpers import row2dict
from image_api import image_api
from listing_api import listing_api
from login_api import login_api
from models import Listing, School, User
from user_api import user_api

application = app = Flask(__name__)
auth = HTTPBasicAuth()
app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg'])


app.register_blueprint(user_api)
app.register_blueprint(listing_api)
app.register_blueprint(login_api)
app.register_blueprint(image_api)

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=80)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
