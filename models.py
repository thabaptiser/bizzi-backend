from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import Column, Float, ForeignKey, Integer, String
from sqlalchemy.orm import backref, relationship

from constants import SECRET_KEY
from database import Base


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(50), unique=True)
    password_hash = Column(String(200))
    email = Column(String(100))
    profile_img = relationship("ProfileImage")
    rating = Column(Float)
    school_id = Column(Integer, ForeignKey('school.id'))

    def __init__(self, username, password):
        self.username = username
        self.password_hash = pwd_context.encrypt(password)
        self.rating = 0

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration = 600):
        s = Serializer(SECRET_KEY, expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def delete_auth_token(self, token):
        s = Serializer(SECRET_KEY)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def get_user_from_token(token):
        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

class ProfileImage(Base):
    __tablename__ = 'profileimage'
    id = Column(Integer, primary_key=True, autoincrement=True)
    location = Column(String(60))
    user_id = Column(Integer, ForeignKey('user.id'))

class ListingImage(Base):
    __tablename__ = 'listingimage'
    id = Column(Integer, primary_key=True, autoincrement=True)
    location = Column(String(60))
    listing_id = Column(Integer, ForeignKey('listing.id'))

class Listing(Base):
    __tablename__ = 'listing'
    id = Column(Integer, primary_key=True, autoincrement=True)
    object_type = Column(String(140), index=True)
    name = Column(String(140))
    description = Column(String(400))
    seller_id = Column(Integer, ForeignKey('user.id'))
    buyer_id = Column(Integer, ForeignKey('user.id'))
    seller = relationship("User", backref='selling', primaryjoin='Listing.seller_id == User.id')
    buyer = relationship("User", backref='bought', primaryjoin='Listing.buyer_id == User.id')
    price = Column(Float)
    images = relationship("ListingImage")

class School(Base):
    __tablename__ = 'school'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50))
    users = relationship("User", backref='school')

    @staticmethod
    def get_object_from_name(name):
        return School.query.filter_by(name = name).first()
