SECRET_KEY = 'ud3mA8VCP0bxg9HxgvBm'
UPLOAD_FOLDER = 'upload/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
THUMBNAIL_SIZES = [[187,281]]
TYPES = ['book', 'tech', 'cloth', 'misc', 'furniture']
