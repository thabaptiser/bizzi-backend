import json
import math
import os
import uuid

from flask import Blueprint, Flask, abort, jsonify, request, send_file
from flask_httpauth import HTTPBasicAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from sqlalchemy import asc, desc
from werkzeug import secure_filename
from werkzeug.security import check_password_hash, generate_password_hash

from constants import ALLOWED_EXTENSIONS, THUMBNAIL_SIZES, UPLOAD_FOLDER
from database import db_session
from helpers import row2dict
from models import Listing, ListingImage, School, User
from PIL import Image

auth = HTTPBasicAuth()

image_api = Blueprint('image_api', __name__)

def make_thumbnails(files):
    for image in files:
        image = os.path.join(UPLOAD_FOLDER, image)
        for size in THUMBNAIL_SIZES:
            if not os.path.isfile(image):
                img_file = open(image, 'wb')
                img_file.flush()
                os.fsync(img_file)
                img_file.close()
            img = Image.open(image)
            img.load()
            img.thumbnail(size)
            img.save("%s_thumbnail_%s.png" % (image, "_".join(map(str, size))))

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    print username_or_token
    user = User.get_user_from_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    return True

def add_listing_image(image, listing):
    filename = str(uuid.uuid4())+'.png'
    while os.path.exists(os.path.join(UPLOAD_FOLDER, filename)):
        filename = str(uuid.uuid4())+'.png'
    image.save(os.path.join(UPLOAD_FOLDER, filename))
    make_thumbnails([filename])
    new_image = ListingImage(location=os.path.join(UPLOAD_FOLDER, filename))
    db_session.add(new_image)
    db_session.commit()
    return new_image

@image_api.route('/api/listing/image/<image_id>')
@auth.login_required
def get_image(image_id):
    image = ListingImage.query.filter_by(id = image_id).first()
    return send_file(image.location, mimetype='image/png')

@image_api.route('/api/listing/image/<image_id>/thumbnail', methods=['POST'])
@auth.login_required
def get_thumbnail(image_id):
    width = request.json.get('width')
    height = request.json.get('height')
    image = ListingImage.query.filter_by(id = image_id).first()
    return send_file("%s_thumbnail_%s_%s.png" % (image.location, width, height), mimetype='image/png')
