import math
import os

from flask import Blueprint, Flask, jsonify, request
from flask_httpauth import HTTPBasicAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from sqlalchemy import asc, desc
from werkzeug.security import check_password_hash, generate_password_hash

from database import db_session
from helpers import row2dict
from models import Listing, School, User

login_api = Blueprint('login_api', __name__)
auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    print username_or_token
    user = User.get_user_from_token(username_or_token)
    print user
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    return True

@login_api.route('/api/login/token', methods=['GET', 'POST'])
@auth.login_required
def get_auth_token():
    user = User.query.filter_by(username = auth.username()).first()
    if user:
        token = user.generate_auth_token()
        return jsonify({ 'token': token.decode('ascii') })
    abort(401)

@login_api.route('/api/login/logout', methods=['GET', 'POST'])
@auth.login_required
def delete_auth_token():
    user = User.query.filter_by(username = auth.username()).first()
    if user:
        token = user.generate_auth_token()
        return jsonify({ 'token': token.decode('ascii') })
    abort(401)

@login_api.route('/api/login/refresh', methods=['GET', 'POST'])
@auth.login_required
def refresh_auth_token():
    user = User.get_user_from_token(auth.username())
    token = user.generate_auth_token()
    return jsonify({'token': token.decode('ascii')})
