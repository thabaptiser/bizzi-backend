import math
import os

from flask import Blueprint, Flask, abort, jsonify, request
from flask_httpauth import HTTPBasicAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from sqlalchemy import asc, desc
from werkzeug.security import check_password_hash, generate_password_hash

from database import db_session
from helpers import row2dict
from models import Listing, School, User

user_api = Blueprint('user_api', __name__)
auth = HTTPBasicAuth()

@user_api.route('/api/user/add', methods=['POST'])
def add_user():
    print request.headers.get('User-Agent')
    if not request.json:
        abort(400)
    username = request.json.get('username')
    password = request.json.get('password')
    school_name = request.json.get('school')
    if username is None or password is None:
        abort(400)
    if User.query.filter_by(username = username).first():
        abort(400)
    new_user = User(username = username, password = password)
    school_name = School.query.filter_by(name=school_name).first()
    if not school_name:
        school = School(name=school_name)
        db_session.add(school)
    school.users.append(new_user)
    db_session.add(new_user)
    db_session.commit()
    return jsonify({'username':new_user.username})
