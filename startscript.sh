#!/bin/bash
until "/home/ec2-user/bizzi/dontrun.sh"; do
    echo "Server 'bizzi' crashed with exit code $?.  Respawning.." >&2
    sleep 1
done
