import json
import math
import os
import uuid

from flask import Blueprint, Flask, abort, jsonify, request, send_file
from flask_httpauth import HTTPBasicAuth
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from sqlalchemy import asc, desc
from werkzeug import secure_filename
from werkzeug.security import check_password_hash, generate_password_hash

from constants import ALLOWED_EXTENSIONS, THUMBNAIL_SIZES, UPLOAD_FOLDER, TYPES
from database import db_session
from helpers import row2dict
from image_api import add_listing_image, make_thumbnails
from models import Listing, ListingImage, School, User
from PIL import Image

auth = HTTPBasicAuth()

listing_api = Blueprint('listing_api', __name__)

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.get_user_from_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username = username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    return True


@listing_api.route('/api/listing/add', methods=['POST'])
@auth.login_required
def add_listing():
    username = auth.username()
    user = User.get_user_from_token(username)
    data = request.form['data']
    form = json.loads(data)
    object_type = form.get('type')
    name = form.get('name')
    description = form.get('description')
    price = form.get('price')
    image = request.files['file']
    if object_type and name and description and price and image and object_type in TYPES:
            listing = Listing(object_type=object_type, name=name, description=description, price=price)
            listing.images.append(add_listing_image(image, listing))
            listing.seller = user
            user.selling.append(listing)
            db_session.add(listing)
            db_session.commit()
    return jsonify({'name': name})

@listing_api.route('/api/listing/image/<image_id>')
@auth.login_required
def get_image(image_id):
    image = ListingImage.query.filter_by(id = image_id).first()
    return send_file(image.location, mimetype='image/png')

@listing_api.route('/api/listing/image/<image_id>/thumbnail')
@auth.login_required
def get_thumbnail(image_id):
    image = ListingImage.query.filter_by(id = image_id).first()
    return send_file(image.location+'.thumbnail', mimetype='image/png')

@listing_api.route('/api/listing/remove', methods=['POST'])
@auth.login_required
def remove_listing():
    listing_id = request.json.get('id')
    listing = Listing.query.filter_by(id = listing_id).first()
    if listing:
        db_session.remove(listing)
        db_session.commit()
        return jsonify({'listing_id': listing_id})
    abort(404)

@listing_api.route('/api/listing/buy', methods=['POST'])
@auth.login_required
def buy_listing():
    listing_id = request.json.get('id')
    listing = Listing.query.filter_by(id = listing_id).first()
    if listing:
        username = auth.username()
        user = User.get_user_from_token(username)
        user.bought.append(listing)
        db_session.commit()
        return jsonify({'listing_id': listing_id})
    abort(404)

@listing_api.route('/api/listing/list/all', methods=['POST'])
@auth.login_required
def list_listings():
    username = auth.username()
    print username
    user = User.get_user_from_token(username)
    school = user.school.name
    page = request.json.get('page')
    listing_type = request.json.get('type')
    listings = Listing.query.filter_by(object_type = listing_type).slice(20 * page, 20 * (page + 1)).all()
    listings_dict={}
    for listing in listings:
        print listing.seller
        listing_dict = {'id':listing.id, 'name':listing.name, 'school':listing.seller.school.name, 'user_name':listing.seller.username, 'description':listing.description, 'price':listing.price, 'image_ids': [image.id for image in listing.images]}
        listings_dict[listing.id] = listing_dict
    print listings
    return jsonify(listings_dict)

@listing_api.route('/api/listing/list/user', methods=['POST', 'GET'])
@auth.login_required
def list_listings_user():
    username = auth.username()
    print username
    user = User.get_user_from_token(username)
    page = request.json.get('page')
    listings = user.selling[20*page:20*(page+1)]
    selling_dict={}
    for listing in listings:
        listing_dict = {'id':listing.id, 'type':listing.object_type, 'name':listing.name, 'school':listing.seller.school.name, 'user_name':listing.seller.username, 'description':listing.description, 'price':listing.price, 'image_ids': [image.id for image in listing.images]}
        selling_dict[listing.id] = listing_dict
    bought_listings = user.bought[20*page:20*(page+1)]
    bought_dict={}
    for listing in bought_listings:
        bought_listing = {'id':listing.id, 'type':listing.object_type, 'name':listing.name, 'school':listing.seller.school.name, 'user_name':listing.seller.username, 'description':listing.description, 'price':listing.price, 'image_ids': [image.id for image in listing.images]}
        bought_dict[listing.id] = bought_listing
    total_dict = {'bought':bought_dict, 'selling':selling_dict}
    return jsonify(total_dict)

@listing_api.route('/api/listing/list/price/asc', methods=['POST'])
@auth.login_required
def list_listings_price_asc():
    username = auth.username()
    user = User.get_user_from_token(username)
    school = user.school.name
    page = request.json.get('page')
    listing_type = request.json.get('type')
    listings = Listing.query.filter_by(object_type = listing_type).order_by(price).slice(20 * page, 20 * (page + 1)).all()
    listings_dict={}
    for listing in listings:
        print listing.seller
        listing_dict = {'id':listing.id, 'name':listing.name, 'school':listing.seller.school.name, 'user_name':listing.seller.username, 'description':listing.description, 'price':listing.price, 'image_ids': [image.id for image in listing.images]}
        listings_dict[listing.id] = listing_dict
    print listings
    return jsonify(listings_dict)

@listing_api.route('/api/listing/list/price/desc', methods=['POST'])
@auth.login_required
def list_listings_price_desc():
    username = auth.username()
    user = User.get_user_from_token(username)
    school = user.school.name
    page = request.json.get('page')
    listing_type = request.json.get('type')
    listings = Listing.query.filter_by(object_type = listing_type).order_by(desc(price)).slice(20 * page, 20 * (page + 1)).all()
    listings_dict={}
    for listing in listings:
        print listing.seller
        listing_dict = {'id':listing.id, 'name':listing.name, 'school':listing.seller.school.name, 'user_name':listing.seller.username, 'description':listing.description, 'price':listing.price, 'image_ids': [image.id for image in listing.images]}
        listings_dict[listing.id] = listing_dict
    print listings
    return jsonify(listings_dict)
